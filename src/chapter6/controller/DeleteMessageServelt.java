package chapter6.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.service.MessageService;

//つぶやき削除
@WebServlet(urlPatterns = { "/deleteMessage" })
public class DeleteMessageServelt extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        int messageId = Integer.parseInt(request.getParameter("messageId"));

        new MessageService().delete(messageId);
        response.sendRedirect("./");

    }
}
